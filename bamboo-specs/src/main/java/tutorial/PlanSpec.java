package tutorial;

import com.atlassian.bamboo.specs.api.BambooSpec;
import com.atlassian.bamboo.specs.api.builders.BambooKey;
import com.atlassian.bamboo.specs.api.builders.BambooOid;
import com.atlassian.bamboo.specs.api.builders.permission.PermissionType;
import com.atlassian.bamboo.specs.api.builders.permission.Permissions;
import com.atlassian.bamboo.specs.api.builders.permission.PlanPermissions;
import com.atlassian.bamboo.specs.api.builders.plan.Job;
import com.atlassian.bamboo.specs.api.builders.plan.Plan;
import com.atlassian.bamboo.specs.api.builders.plan.PlanIdentifier;
import com.atlassian.bamboo.specs.api.builders.plan.Stage;
import com.atlassian.bamboo.specs.api.builders.plan.artifact.Artifact;
import com.atlassian.bamboo.specs.api.builders.plan.branches.BranchCleanup;
import com.atlassian.bamboo.specs.api.builders.plan.branches.PlanBranchManagement;
import com.atlassian.bamboo.specs.api.builders.plan.configuration.ConcurrentBuilds;
import com.atlassian.bamboo.specs.api.builders.project.Project;
import com.atlassian.bamboo.specs.builders.task.CheckoutItem;
import com.atlassian.bamboo.specs.builders.task.ScriptTask;
import com.atlassian.bamboo.specs.builders.task.VcsCheckoutTask;
import com.atlassian.bamboo.specs.builders.trigger.RepositoryPollingTrigger;

@BambooSpec
public class PlanSpec {
    
    public Plan plan() {
        return new Plan(new Project()
                .oid(new BambooOid("ozidv3d9qsjl"))
                .key(new BambooKey("BEGO"))
                .name("Bego"),
            "demo",
            new BambooKey("DEMO"))
            .oid(new BambooOid("oz8oni01x05d"))
            .pluginConfigurations(new ConcurrentBuilds())
            .stages(new Stage("Default Stage")
                    .jobs(new Job("Default Job",
                            new BambooKey("JOB1"))
                            .artifacts(new Artifact()
                                    .name("Application JAR")
                                    .copyPattern("demo-0.0.1-SNAPSHOT.jar")
                                    .location("target")
                                    .shared(true)
                                    .required(true))
                            .tasks(new VcsCheckoutTask()
                                    .description("Checkout Default Repository")
                                    .checkoutItems(new CheckoutItem().defaultRepository()),
                                new ScriptTask()
                                    .description("Maven Build")
                                    .inlineBody("chmod +x ./mvnw\n./mvnw clean verify"))))
            .linkedRepositories("demo")

            .triggers(new RepositoryPollingTrigger())
            .planBranchManagement(new PlanBranchManagement()
                    .delete(new BranchCleanup())
                    .notificationForCommitters());
    }
    
    public PlanPermissions planPermission() {
        return new PlanPermissions(new PlanIdentifier("BEGO", "DEMO"))
            .permissions(new Permissions()
                    .userPermissions("bamboo", PermissionType.EDIT, PermissionType.VIEW, PermissionType.ADMIN, PermissionType.CLONE, PermissionType.BUILD));
    }
    
    public static void main(String... argv) {
        PlanSpec planSpec = new PlanSpec();
        Utils.publish(planSpec.plan(), planSpec.planPermission());
    }
}
