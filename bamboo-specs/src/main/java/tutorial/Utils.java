package tutorial;

import com.atlassian.bamboo.specs.api.builders.permission.PlanPermissions;
import com.atlassian.bamboo.specs.api.builders.plan.Plan;
import com.atlassian.bamboo.specs.util.BambooServer;

public final class Utils {

    private Utils() {
        throw new AssertionError("Utility class - do not instantiate.");
    }

    static void publish(Plan plan, PlanPermissions planPermission) {
        //By default credentials are read from the '.credentials' file.
        BambooServer bambooServer = new BambooServer("http://172.23.0.2:8085");
        bambooServer.publish(plan);
        bambooServer.publish(planPermission);
    }
}
